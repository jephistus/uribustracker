package edu.uri.uribustracker;
import java.util.HashMap;
/**
 * Created by ray on 5/4/16.
 */
public class BusStore extends HashMap<String, Bus> {
    private static BusStore instance;
    public static BusStore getInstance() {
        if (instance == null) instance = new BusStore();
        return instance;
    }
}
