package edu.uri.uribustracker;

import org.junit.Test;

import static org.junit.Assert.*;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by ray on 4/25/16.
 */
public class RouteTest {
    @Test
    public void testClosestPoint() throws Exception {
        double coords[] = {0, 0, 0.5, 0.5};
        Route r = new Route(coords, 1, 16.0f);
        LatLng l = new LatLng(1, 1);
        Route.InterpolationResult result = r.closestPoint(l);
        assertEquals(result.point.latitude - 0.5 < 1e-5, true);
        assertEquals(result.point.longitude - 0.5 < 1e-5, true);
    }
}
